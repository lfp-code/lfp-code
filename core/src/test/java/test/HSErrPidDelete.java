package test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import com.lfp.code.core.services.Dirs;

import org.apache.commons.lang3.StringUtils;

public class HSErrPidDelete {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		List<String> filters = List.of("hs_err_pid*");
		var project = Dirs.getProjectDir();
		var workspace = project.getParentFile();
		var stream = Dirs.streamFiles(workspace, filters.toArray(String[]::new));
		stream = stream.filter(v -> {
			if (!StringUtils.endsWithIgnoreCase(v.getName(), ".log"))
				return false;
			return true;
		});
		stream.forEach(v -> {
			System.out.println("deleting:" + v.getAbsolutePath());
			v.delete();
		});
		System.err.println("done");
	}

}
