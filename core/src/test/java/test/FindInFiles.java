package test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import com.lfp.code.core.services.Dirs;

import org.apache.commons.lang3.StringUtils;

public class FindInFiles {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		String lookup = "<artifactId>captcha-utils</artifactId>";
		List<String> filters = List.of("pom.xml");
		var project = Dirs.getProjectDir();
		var projectPath = project.getAbsolutePath();
		var workspace = project.getParentFile();
		var stream = Dirs.streamFiles(workspace, filters.toArray(String[]::new));
		stream = stream.filter(v -> {
			var path = v.getAbsolutePath();
			return !path.startsWith(projectPath);
		});
		var pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		var futures = stream.map(v -> {
			var future = CompletableFuture.supplyAsync(() -> {
				try {
					return checkFile(lookup, v);
				} catch (IOException e) {
					throw new CompletionException(e);
				}
			}, pool);
			future.whenComplete((fileOp, t) -> {
				if (t != null)
					logger.error("error during lookup:{} file:{}", lookup, v);
				else if (fileOp.isPresent())
					logger.info(fileOp.get().getAbsolutePath());
			});
			return future;
		}).toList();
		CompletableFuture.allOf(futures.toArray(CompletableFuture[]::new)).get();
		pool.shutdownNow();
		System.err.println("done");
	}

	private static Optional<File> checkFile(String lookup, File file) throws IOException {
		var str = Files.readString(file.toPath(), StandardCharsets.UTF_8);
		if (StringUtils.containsIgnoreCase(str, lookup))
			return Optional.of(file);
		return Optional.empty();
	}

}
