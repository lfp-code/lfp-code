package com.lfp.code.core.services;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractIterator<X> implements Iterator<X> {
	private final AtomicLong index = new AtomicLong();
	private Optional<Optional<X>> next;

	@Override
	public boolean hasNext() {
		synchronized (index) {
			while (next == null) {
				var nextValue = computeNext(index.incrementAndGet());
				if (next == null)
					next = Optional.of(Optional.ofNullable(nextValue));
			}
		}
		return next.isPresent();
	}

	@Override
	public X next() {
		synchronized (index) {
			if (!hasNext())
				throw new NoSuchElementException();
			var result = next.get().orElse(null);
			next = null;
			return result;
		}
	}

	protected X endOfData() {
		next = Optional.empty();
		return null;
	}

	protected abstract X computeNext(long index);

}
