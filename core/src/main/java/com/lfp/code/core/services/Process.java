package com.lfp.code.core.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import one.util.streamex.StreamEx;

public class Process {

	public static StreamEx<String> executeStream(String cmd) throws IOException {
		ProcessBuilder pb;
		if (System.getProperty("os.name").toLowerCase().contains("windows")) {
			pb = new ProcessBuilder("cmd.exe", "/c", cmd);
		} else {
			pb = new ProcessBuilder("/bin/bash", cmd);
		}
		java.lang.Process proc = pb.start();
		var queue = new LinkedBlockingQueue<Optional<CompletableFuture<String>>>();
		var stdReadFuture = read(queue, proc.getInputStream());
		var errReadFuture = read(queue, proc.getErrorStream());
		CompletableFuture.allOf(stdReadFuture, errReadFuture).whenComplete((v, t) -> {
			proc.onExit().whenComplete((nilk, nilv) -> {
				int exitVal = proc.exitValue();
				if (exitVal != 0)
					queue.add(Optional.of(failedFuture(new IOException("non zero exit code"))));
				queue.add(Optional.empty());
			});
		});
		StreamEx<CompletableFuture<String>> stream = StreamEx.produce(action -> {
			Optional<CompletableFuture<String>> nextOp;
			try {
				nextOp = queue.take();
			} catch (InterruptedException e) {
				throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
			}
			if (nextOp.isEmpty())
				return false;
			action.accept(nextOp.get());
			return true;
		});
		return stream.map(CompletableFuture::join);
	}

	public static String execute(String cmd) throws IOException {
		return executeStream(cmd).joining("\n");
	}

	private static CompletableFuture<Void> read(BlockingQueue<Optional<CompletableFuture<String>>> queue,
			InputStream inputStream) {
		var executor = Executors.newFixedThreadPool(1);
		var future = CompletableFuture.runAsync(() -> {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));) {
				br.lines().forEach(line -> {
					queue.add(Optional.of(CompletableFuture.completedFuture(line)));
				});
			} catch (Throwable t) {
				queue.add(Optional.of(failedFuture(t)));
			}
		}, executor);
		future = future.whenComplete((v, t) -> {
			try {
				inputStream.close();
			} catch (IOException e) {
				throw new CompletionException(e);
			}
		});
		return future;
	}

	private static <T> CompletableFuture<T> failedFuture(Throwable t) {
		var failureFuture = new CompletableFuture<T>();
		failureFuture.completeExceptionally(t);
		return failureFuture;
	}

	public static void main(String[] args) throws IOException {
		System.out.println(Process.execute("java --version"));
		System.out.println(Process.execute("echo hi"));
	}

}
