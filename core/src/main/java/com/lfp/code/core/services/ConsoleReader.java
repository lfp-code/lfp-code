package com.lfp.code.core.services;

import java.util.Scanner;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import one.util.streamex.StreamEx;

public enum ConsoleReader {
	INSTANCE;

	private final Supplier<Scanner> CONSOLE_SCANNER_S = Suppliers.memoize(() -> new Scanner(System.in));

	public String readConsoleLine() {
		return readConsoleLine(null);
	}

	public String readConsoleLine(String prompt) {
		return readConsoleLine(prompt, false);
	}

	public String readConsoleLine(String prompt, boolean allowBlank) {
		var resultOp = streamConsoleLines(prompt, allowBlank).findFirst();
		if (!allowBlank)
			return resultOp.get();
		return resultOp.orElse(null);
	}

	public StreamEx<String> streamConsoleLines(String prompt) {
		return streamConsoleLines(prompt, false);
	}

	public StreamEx<String> streamConsoleLines(String prompt, boolean allowBlank) {
		if (StringUtils.isNotBlank(prompt))
			System.out.println(prompt);
		var iter = new AbstractIterator<String>() {

			@Override
			protected String computeNext(long index) {
				String line = CONSOLE_SCANNER_S.get().nextLine();
				if (line.isEmpty())
					return this.endOfData();
				return line;
			}
		};
		var stream = StreamEx.of(iter);
		if (!allowBlank)
			stream = stream.map(v -> {
				Validate.isTrue(StringUtils.isNotBlank(v), "blank input:%s", v);
				return v;
			});
		return stream;
	}
}
