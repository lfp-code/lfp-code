package com.lfp.code.core.services;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import one.util.streamex.StreamEx;

public class Dirs {
	private final static String RESOURCES_DIR = "src/main/resources";

	public static List<File> getFoldersInDir(File parent) {
		List<File> folders = new ArrayList<File>();
		for (File file : parent.listFiles()) {
			if (file.isDirectory()) {
				folders.add(file);
			}
		}
		return folders;
	}

	public static boolean containsGit(File folder) {
		for (File sub : Dirs.getFoldersInDir(folder)) {
			if (sub.isDirectory() && StringUtils.equalsIgnoreCase(sub.getName(), ".git")) {
				return true;
			}
		}
		return false;
	}

	public static List<File> lookupGitFolders(File parent) {
		List<File> gits = new ArrayList<File>();
		List<File> folders = Dirs.getFoldersInDir(parent);
		for (File folder : folders) {
			if (Dirs.containsGit(folder)) {
				gits.add(folder);
			}
		}
		return gits;
	}

	public static boolean validAndMatchesPattern(File file, List<String> patterns) throws IOException {
		boolean result = false;
		if (patterns != null && patterns.size() > 0) {
			for (String patt : patterns) {
				patt = patt.trim().toLowerCase();
				String pattNoAsterik;
				{
					pattNoAsterik = patt.startsWith("*") ? patt.substring(1) : patt;
					pattNoAsterik = patt.endsWith("*") ? patt.substring(0, patt.length() - 1) : patt;
				}
				if (patt.equals("*")) {
					result = true;
				} else if (patt.endsWith("*") && patt.startsWith("*")
						&& file.getName().toLowerCase().contains(pattNoAsterik)) {
					return true;
				} else if (patt.endsWith("*") && file.getName().toLowerCase().startsWith(pattNoAsterik)) {
					result = true;
				} else if (patt.startsWith("*") && file.getName().toLowerCase().endsWith(pattNoAsterik)) {
					result = true;
				} else if (patt.equals(file.getName().toLowerCase())) {
					result = true;
				}
				if (result) {
					break;
				}
			}

		}
		if (result && Dirs.getProjectDir().getCanonicalPath().equals(file.getCanonicalPath())) {
			System.err.println("skipping: " + Dirs.getProjectDir()
					+ ". folder would have matched but can't perform actions on self");
			return false;
		}
		return result;
	}

	public static File currentDir() {
		var file = new File(System.getProperty("user.dir"));
		return file;
	}

	public static File getProjectDir() {
		return getProjectDir(currentDir());
	}

	public static File getProjectDir(File file) {
		var parent = file.getParentFile();
		while (parent != null && new File(parent, "pom.xml").exists()) {
			file = parent;
			parent = file.getParentFile();
		}
		return file;
	}

	public static File loadFileResource(String path) {
		return new File(new File(RESOURCES_DIR).getAbsolutePath() + File.separator + path);
	}

	public static boolean sameFile(File file1, File file2) {
		try {
			return file1.getCanonicalPath().equals(file2.getCanonicalPath());
		} catch (IOException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	public static StreamEx<File> streamWalkFiles(File parentDirectory) throws IOException {
		if (parentDirectory == null || !parentDirectory.exists())
			return StreamEx.empty();
		Path start = parentDirectory.toPath();
		Stream<Path> stream = Files.walk(start, Integer.MAX_VALUE);
		return StreamEx.of(stream).map(Path::toFile);

	}

	public static StreamEx<File> streamFiles(File file, String... filters) {
		if (file == null)
			return StreamEx.empty();
		StreamEx<String> filterStream = StreamEx.of(filters).nonNull().flatMap(v -> {
			return Arrays.asList(StringUtils.split(v)).stream();
		});
		filterStream = filterStream.map(StringUtils::trimToNull).nonNull().distinct();
		var filterList = filterStream.toList();
		Predicate<Path> filterPredicate = p -> {
			if (filterList.isEmpty())
				return true;
			var f = p.toFile();
			for (var filter : filterList) {
				filter = StringUtils.trimToNull(filter);
				if (filter == null)
					continue;
				boolean wildStart = StringUtils.startsWithIgnoreCase(filter, "*");
				if (wildStart)
					filter = filter.substring(1);
				boolean wildEnd = StringUtils.endsWithIgnoreCase(filter, "*");
				if (wildEnd)
					filter = filter.substring(0, filter.length() - 1);
				var name = f.getName();
				if (StringUtils.equalsIgnoreCase(name, filter))
					return true;
				if (wildStart && wildEnd && StringUtils.containsIgnoreCase(name, filter))
					return true;
				if (wildStart && StringUtils.endsWithIgnoreCase(name, filter))
					return true;
				if (wildEnd && StringUtils.startsWithIgnoreCase(name, filter))
					return true;
			}
			return false;
		};
		StreamEx<Path> pathStream;
		try {
			pathStream = StreamEx.of(Files.walk(file.toPath()).parallel().filter(Files::isRegularFile));
		} catch (IOException e) {
			throw java.util.Optional.ofNullable(e).filter(java.lang.RuntimeException.class::isInstance)
					.map(java.lang.RuntimeException.class::cast).orElseGet(java.lang.RuntimeException::new);
		}
		pathStream = pathStream.filter(filterPredicate);
		return pathStream.map(v -> v.toFile());
	}

	public static File tmpDir(Object... append) {
		var tempFile = new File(System.getProperty("java.io.tmpdir"));
		var appendPath = append == null ? null : StreamEx.of(append).map(v -> {
			var str = v == null ? null : v.toString();
			return URLEncoder.encode(str, StandardCharsets.UTF_8);
		}).joining(File.separator);
		if (StringUtils.isBlank(appendPath))
			return tempFile;
		return new File(tempFile, appendPath);
	}
}
