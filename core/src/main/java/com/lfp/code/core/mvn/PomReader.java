package com.lfp.code.core.mvn;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;

import com.lfp.code.core.services.AbstractIterator;
import com.lfp.code.core.services.Suppliers;

import one.util.streamex.StreamEx;

public class PomReader {

	private static Map<String, Optional<PomReader>> CACHE = new ConcurrentHashMap<>();

	public static Optional<PomReader> get(File pomFile) {
		return get(pomFile, false);
	}

	public static Optional<PomReader> get(File pomFile, boolean refreshCaching) {
		if (pomFile == null)
			return Optional.empty();
		Optional<PomReader> op = CACHE.compute(pomFile.getAbsolutePath(), (k, v) -> {
			if (v != null && !refreshCaching)
				return v;
			if (!pomFile.exists() || pomFile.length() <= 0)
				return Optional.empty();
			return Optional.of(new PomReader(pomFile, null, refreshCaching));
		});
		return op;
	}

	private static final MavenXpp3Reader MAVEN_READER = new MavenXpp3Reader();
	private final Supplier<Iterable<PomReader>> modulesSupplier = Suppliers.memoize(() -> getModulesFresh());
	private final Supplier<String> canonicalPathSupplier = Suppliers.memoize(() -> {
		try {
			return getPomFile().getCanonicalPath();
		} catch (IOException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	});
	private final File pomFile;
	private final Model model;
	private PomReader parentPomReader;
	private boolean refreshCaching;

	private PomReader(File pomFile, PomReader parentPomReader, boolean refreshCaching) {
		this.refreshCaching = refreshCaching;
		this.pomFile = Objects.requireNonNull(pomFile);
		if (parentPomReader != null)
			this.parentPomReader = parentPomReader;
		else
			this.parentPomReader = lookupParentPomReader();
		Validate.isTrue(pomFile.exists());
		try {
			this.model = MAVEN_READER.read(new FileReader(pomFile));
		} catch (Exception e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	public void write() throws IOException {
		MavenXpp3Writer mavenWriter = new MavenXpp3Writer();
		Writer writer = new FileWriter(pomFile.getAbsolutePath());
		mavenWriter.write(writer, model);
	}

	private void writeUnchecked() {
		try {
			write();
		} catch (IOException e) {
			throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
					? java.lang.RuntimeException.class.cast(e)
					: new java.lang.RuntimeException(e);
		}
	}

	private PomReader lookupParentPomReader() {
		File parentPom = new File(this.pomFile.getParentFile().getParentFile().getAbsolutePath() + "/pom.xml");
		if (!parentPom.exists())
			return null;
		Optional<PomReader> parentPomReaderCheck = PomReader.get(parentPom, refreshCaching);
		if (!parentPomReaderCheck.isPresent())
			return null;
		for (PomReader prm : parentPomReaderCheck.get().getModules())
			if (prm.getPomFile().equals(this.pomFile)) {
				return parentPomReaderCheck.get();
			}
		return null;
	}

	public boolean containsDependency(PomReader pomReader) {
		return getDependencies(pomReader).size() > 0;
	}

	public List<Dependency> getDependencies(PomReader lookupPomReader) {
		Set<Dependency> res = new LinkedHashSet<>();
		for (PomReader lookupPomReaderSub : lookupPomReader.streamAll()) {
			String prArtifactId = lookupPomReaderSub.getArtifactId();
			String prGroupId = lookupPomReaderSub.getGroupId();
			for (PomReader pr : this.streamAll()) {
				StreamEx<Dependency> depStream = StreamEx.of(pr.getDependencies().iterator());
				for (Dependency dep : depStream) {
					String gid = getDecodedValue(dep.getGroupId(), pr);
					if (!StringUtils.equals(gid, prGroupId))
						continue;
					String aid = getDecodedValue(dep.getArtifactId(), pr);
					if (!StringUtils.equals(aid, prArtifactId))
						continue;
					res.add(dep);
				}
			}
		}
		return new ArrayList<>(res);
	}

	public Iterable<Dependency> getDependencies() {
		List<Dependency> deps = this.model.getDependencies();
		return deps == null ? Collections.emptyList() : deps;
	}

	public Map<String, Set<String>> getGroupIdArtifactIdMap(boolean includeModules) {
		Map<String, Set<String>> deps = new LinkedHashMap<>();
		for (PomReader pr : !includeModules ? Arrays.asList(this) : this.streamAll()) {
			for (Dependency dep : pr.getDependencies()) {
				String groupId = getDecodedValue(dep.getGroupId(), pr);
				String artifactId = getDecodedValue(dep.getArtifactId(), pr);
				deps.computeIfAbsent(groupId, nil -> new HashSet<String>()).add(artifactId);
			}
		}
		return deps;
	}

	@SuppressWarnings("resource")
	public boolean setVersion(String version) {
		if (Objects.equals(version, getVersion()))
			return false;
		for (PomReader pr : this.streamToRoot()) {
			String prVersion = pr.getModel().getVersion();
			if (StringUtils.isBlank(prVersion))
				continue;
			Map<String, Boolean> map = getEncodedKeyMap(prVersion);
			Validate.isTrue(map.size() == 1, "can't set version when multiple key maps used:%s", map);
			boolean updateProperty = map.values().iterator().next();
			if (!updateProperty) {
				pr.model.setVersion(version);
				if (pr != this)
					pr.writeUnchecked();
				return true;
			} else {
				return setFirstProperty(pr, map.keySet().iterator().next(), version, pr != this);
			}
		}
		return false;
	}

	public boolean setDependencyVersion(PomReader pomReader, String version) {
		boolean mod = false;
		for (PomReader pr : this.streamAll()) {
			List<Dependency> dependencies = pr.getDependencies(pomReader);
			if (dependencies.isEmpty())
				continue;
			boolean needsWrite = true;
			for (Dependency dep : dependencies) {
				String depVer = dep.getVersion();
				if (depVer == null)
					continue;
				Map<String, Boolean> map = getEncodedKeyMap(depVer);
				Validate.isTrue(map.size() == 1, "can't set version when multiple key maps used:%s", map);
				boolean updateProperty = map.values().iterator().next();
				if (!updateProperty) {
					dep.setVersion(version);
					needsWrite = true;
					mod = true;
				} else {
					if (setFirstProperty(pr, map.keySet().iterator().next(), version, pr != this)) {
						mod = true;
					}
				}
				if (needsWrite && pr != this)
					pr.writeUnchecked();
			}
		}
		return mod;
	}

	public PomReader getRootPomReader() {
		PomReader root = this;
		PomReader parent;
		do {
			parent = root.getParentPomReader();
			if (parent != null)
				root = parent;
		} while (parent != null);
		return root;
	}

	public StreamEx<PomReader> streamToRoot() {
		Iterator<PomReader> parentIter = new AbstractIterator<PomReader>() {
			private PomReader next = PomReader.this;

			@Override
			protected PomReader computeNext(long index) {
				if (next == null)
					return this.endOfData();
				PomReader res = next;
				next = res.getParentPomReader();
				return res;
			}
		};
		return StreamEx.of(parentIter);
	}

	public StreamEx<PomReader> streamAll() {
		StreamEx<PomReader> stream = StreamEx.of(this);
		for (PomReader module : this.getModules()) {
			stream = stream.append(module.streamAll());
		}
		return stream;
	}

	public Iterable<PomReader> getModules() {
		return modulesSupplier.get();
	}

	private Iterable<PomReader> getModulesFresh() {
		List<String> modules = this.model.getModules();
		if (modules == null || modules.isEmpty())
			return Collections.emptyList();
		List<PomReader> pomInfos = new ArrayList<>();
		for (String module : modules) {
			File modulePom = new File(pomFile.getParentFile().getAbsolutePath() + "/" + module + "/pom.xml");
			if (modulePom.exists())
				pomInfos.add(new PomReader(modulePom, this, refreshCaching));
		}
		return Collections.unmodifiableList(pomInfos);
	}

	public Date getLastModified() {
		long updated = this.getPomFile().lastModified();
		for (PomReader module : this.getModules()) {
			long moduleUpdated = module.getPomFile().lastModified();
			if (moduleUpdated > updated)
				updated = moduleUpdated;
		}
		return new Date(updated);
	}

	@SuppressWarnings("resource")
	public String getGroupId() {
		for (PomReader pr : streamToRoot()) {
			String gid = getDecodedValue(pr.getModel().getGroupId(), pr);
			if (StringUtils.isNotBlank(gid))
				return gid;
		}
		return null;
	}

	@SuppressWarnings("resource")
	public String getArtifactId() {
		for (PomReader pr : streamToRoot()) {
			String aid = getDecodedValue(pr.getModel().getArtifactId(), pr);
			if (StringUtils.isNotBlank(aid))
				return aid;
		}
		return null;
	}

	public String getFolderName() {
		return this.getPomFile().getParentFile().getName();
	}

	@SuppressWarnings("resource")
	public String getVersion() {
		for (PomReader pr : this.streamToRoot()) {
			String ver = pr.getModel().getProperties().getProperty("revision");
			if (StringUtils.isBlank(ver))
				ver = getDecodedValue(pr.getModel().getVersion(), pr);
			if (!StringUtils.isBlank(ver))
				return ver;
		}
		return null;
	}

	public String getProperty(String key) {
		Properties properties = this.model.getProperties();
		if (properties == null)
			return null;
		return properties.getProperty(key);
	}

	@SuppressWarnings("resource")
	private static boolean setFirstProperty(PomReader pomReader, String name, String value, boolean write) {
		if (pomReader == null)
			return false;
		for (PomReader pr : pomReader.streamToRoot()) {
			Properties props = pr.model.getProperties();
			String cur = props == null ? null : props.getProperty(name);
			if (StringUtils.isBlank(cur))
				continue;
			if (cur.equals(value))
				continue;
			props.setProperty(name, value);
			if (write)
				pr.writeUnchecked();
			return true;
		}
		return false;
	}

	@SuppressWarnings("resource")
	public static String getDecodedValue(String value, PomReader pomReader) {
		if (value == null)
			return value;
		Map<String, Boolean> map = getEncodedKeyMap(value);
		StreamEx<String> decodeStream = StreamEx.of(map.keySet()).map(k -> {
			boolean isProp = map.get(k);
			if (!isProp)
				return k;
			else {
				for (PomReader pr : pomReader.streamToRoot()) {
					Properties props = pr.model.getProperties();
					String decoded = props == null ? null : props.getProperty(k);
					if (StringUtils.isNotBlank(decoded))
						return getDecodedValue(decoded, pr);
				}
			}
			return null;
		});
		return decodeStream.collect(Collectors.joining());
	}

	private static Map<String, Boolean> getEncodedKeyMap(String value) {
		java.util.Objects.requireNonNull(value, "value is not present");
		if (!StringUtils.contains(value, "${") || !StringUtils.contains(value, "}"))
			return Map.of(value, false);
		Stream<String> stream = Arrays.asList(StringUtils.split(value, "${")).stream();
		stream = stream.filter(StringUtils::isNotBlank);
		Map<String, Boolean> result = new LinkedHashMap<>();
		stream.forEach(s -> {
			if (!StringUtils.contains(s, "}")) {
				result.put(s, false);
				return;
			}
			String pre = StringUtils.substringBefore(s, "}");
			if (StringUtils.isBlank(pre)) {
				result.put(s, false);
				return;
			}
			result.put(pre, true);
		});
		return Map.copyOf(result);
	}

	public File getPomFile() {
		return pomFile;
	}

	public String getCanonicalPath() {
		return canonicalPathSupplier.get();
	}

	public Model getModel() {
		return model;
	}

	public PomReader getParentPomReader() {
		return parentPomReader;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.getCanonicalPath().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PomReader other = (PomReader) obj;
		return java.util.Objects.equals(this.getCanonicalPath(), other.getCanonicalPath());
	}

	@Override
	public String toString() {
		return "PomReader [" + getFolderName() + "]";
	}

}
