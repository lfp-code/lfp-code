package com.lfp.code.core.services;

import okhttp3.OkHttpClient;

public enum OkHttp {
	INSTANCE;

	private final OkHttpClient client = new OkHttpClient.Builder().build();

	public OkHttpClient getClient() {
		return client;
	}

}
