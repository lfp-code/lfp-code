package com.lfp.code.core.services;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.google.gson.Gson;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okio.BufferedSink;
import okio.Okio;

public enum EmailLogger {
    INSTANCE;

    private static final Class<?> THIS_CLASS = new Object() {
    }.getClass().getEnclosingClass();
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
    private static final Gson GSON = new Gson();

    @SuppressWarnings("unchecked")
    public boolean sendMessage(String toAddress, String subject, String messageHtml, File... attachments)
            throws IOException {
        var attachmentMap = new LinkedHashMap<String, RequestBody>();
        if (attachments != null)
            for (var attachment : attachments) {
                if (attachment == null || !attachment.exists())
                    continue;
                String mimeType = Optional.ofNullable(Files.probeContentType(attachment.toPath())).orElse("text/plain");
                attachmentMap.put(attachment.getName(), new RequestBody() {

                    @Override
                    public void writeTo(BufferedSink sink) throws IOException {
                        try (FileInputStream is = new FileInputStream(attachment)) {
                            sink.writeAll(Okio.source(is));
                        }
                    }

                    @Override
                    public MediaType contentType() {
                        return MediaType.parse(mimeType);
                    }
                });
            }
        return sendMessage(toAddress, subject, messageHtml, attachmentMap);
    }

    public boolean sendMessage(String subject, String messageHtml, Map<String, RequestBody> attachments)
            throws IOException {
        return sendMessage(null, subject, messageHtml, attachments);
    }

    @SuppressWarnings({"unchecked"})
    public boolean sendMessage(String toAddress, String subject, String messageHtml,
                               Map<String, RequestBody> attachments) throws IOException {
        if (StringUtils.isBlank(toAddress))
            toAddress = ConfigPrivateProperties.INSTANCE.getProperties().getProperty("email-logger-to-address");
        String apiKey = ConfigPrivateProperties.INSTANCE.getProperties().getProperty("mailgun-api-key");
        String fromAddress = ConfigPrivateProperties.INSTANCE.getProperties().getProperty("email-logger-from-address");
        Map<String, String> data = new LinkedHashMap<>();
        {
            data.put("to", toAddress);
            data.put("from", fromAddress);
            data.put("subject", subject);
            data.put("html", messageHtml);
        }
        String errorMessage;
        if (StringUtils.isBlank(apiKey))
            errorMessage = "blank apiKey";
        if (StringUtils.isBlank(toAddress))
            errorMessage = "blank toAddress";
        if (StringUtils.isBlank(fromAddress))
            errorMessage = "blank fromAddress";
        else
            errorMessage = null;
        if (errorMessage != null) {
            String json = new GsonBuilder().setPrettyPrinting().create().toJson(data);
            logger.info("unable to send email. error:'{}' data:\n{}", errorMessage, json);
            return false;
        }
        var rb = new Request.Builder().url("https://api.mailgun.net"
                + String.format("/v3/%s/messages", StringUtils.substringAfterLast(fromAddress, "@")));
        {
            String authValue = new String(Base64.getEncoder().encode(("api:" + apiKey).getBytes()));
            rb.header("Authorization", "Basic " + authValue);
        }
        var mpb = new MultipartBody.Builder().setType(MultipartBody.FORM);
        data.entrySet().forEach(ent -> mpb.addFormDataPart(ent.getKey(), ent.getValue()));
        if (attachments != null) {
            for (var attachmentEntry : attachments.entrySet()) {
                mpb.addFormDataPart("attachment", attachmentEntry.getKey(), attachmentEntry.getValue());
            }
        }
        rb.post(mpb.build());
        String json;
        try (var response = OkHttp.INSTANCE.getClient().newCall(rb.build()).execute()) {
            Validate.isTrue(response.isSuccessful(), "invalid response:%s", response);
            json = response.body().string();
        }
        Map<String, Object> map = GSON.fromJson(json, Map.class);
        Validate.isTrue(StringUtils.isNotBlank(Objects.toString(map.get("id"))), "error:" + json);
        return true;
    }

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws IOException, InterruptedException {
        EmailLogger.INSTANCE.sendMessage("reggie.pierce@iplasso.com", "test message w/ attachment",
                "the build process started", Map.of("file-1.txt", new RequestBody() {

                    @Override
                    public MediaType contentType() {
                        return MediaType.get("text/plain");
                    }

                    @Override
                    public void writeTo(BufferedSink sink) throws IOException {
                        var is = new ByteArrayInputStream("this is an attachment".getBytes());
                        sink.writeAll(Okio.source(is));
                    }
                }));
        Thread.sleep(1000);
        System.err.println("done");
    }

}
