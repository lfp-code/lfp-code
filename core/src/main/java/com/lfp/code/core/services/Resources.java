package com.lfp.code.core.services;

import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Optional;

public class Resources {

	private Resources() {
	}

	public static InputStream get(String path) {
		return Resources.class.getClassLoader().getResourceAsStream(path);
	}

	public static Optional<File> getFile(String path) {
		URL url = Resources.class.getClassLoader().getResource(path);
		if (url == null)
			return Optional.empty();
		try {
			return Optional.of(new File(url.toURI()));
		} catch (URISyntaxException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
	}
}
