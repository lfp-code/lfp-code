package com.lfp.code.core.services;

import java.util.function.Supplier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lfp.code.core.services.Suppliers;

public class Gsons {

	private Gsons() {
	}

	private static final Supplier<Gson> GSON_S = Suppliers.memoize(() -> getBuilder().create());
	private static final Supplier<Gson> GSON_PRETTY_S = Suppliers
			.memoize(() -> getBuilder().setPrettyPrinting().create());

	public static Gson get() {
		return GSON_S.get();
	}

	public static Gson getPretty() {
		return GSON_PRETTY_S.get();
	}

	public static GsonBuilder getBuilder() {
		var gsb = new GsonBuilder();
		return gsb;
	}
}
