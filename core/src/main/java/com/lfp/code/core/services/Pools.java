package com.lfp.code.core.services;

import java.time.Duration;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import one.util.streamex.IntStreamEx;

public class Pools {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private static final Supplier<ExecutorService> UNLIMITED_S = Suppliers.memoize(() -> {
		var threadFactoryDelegate = Executors.defaultThreadFactory();
		ThreadFactory threadFactory = r -> {
			Thread t = threadFactoryDelegate.newThread(r);
			t.setDaemon(false);
			return t;
		};
		Duration threadTimeout = Duration.ofSeconds(15);
		ExecutorService executor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(), Integer.MAX_VALUE,
				threadTimeout.toMillis(), TimeUnit.MILLISECONDS, new SynchronousQueue<Runnable>(), threadFactory);
		return executor;
	});

	public static ExecutorService unlimited() {
		return UNLIMITED_S.get();
	}

	public static Executor limited() {
		return limited(Runtime.getRuntime().availableProcessors());
	}

	public static Executor limited(int parallelism) {
		Executor executor = new Executor() {

			private final Semaphore throttle = new Semaphore(parallelism, true);
			private final BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

			@Override
			public void execute(Runnable command) {
				queue.add(command);
				queueExecute();
			}

			private void queueExecute() {
				if (!throttle.tryAcquire())
					return;
				try {
					queueExecute(queue.poll());
				} catch (Throwable t) {
					throttle.release();
					throw t;
				}
			}

			private void queueExecute(Runnable command) {
				if (command == null) {
					throttle.release();
					return;
				}
				var future = CompletableFuture.runAsync(command, unlimited());
				future.whenCompleteAsync((v, t) -> {
					throttle.release();
					queueExecute();
				}, unlimited());
			}
		};
		return executor;
	}

	public static void main(String[] args) {
		var pool = Pools.limited();
		var futures = IntStreamEx.range(100).mapToObj(v -> {
			return CompletableFuture.supplyAsync(() -> {
				System.out.println("starting:" + v);
				try {
					Thread.sleep(Duration.ofSeconds(1).toMillis());
				} catch (InterruptedException e) {
					throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e)
							: new RuntimeException(e);
				}
				System.out.println("completed:" + v);
				return v;
			}, pool);
		}).toArray(CompletableFuture.class);
		CompletableFuture.allOf(futures).join();
		System.err.println("done");
	}
}
