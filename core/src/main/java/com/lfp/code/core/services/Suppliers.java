package com.lfp.code.core.services;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

public class Suppliers {

	public static <X> Supplier<X> memoize(Supplier<X> supplier) {
		Objects.requireNonNull(supplier);
		return new Supplier<X>() {

			private Optional<X> valueOp;

			@Override
			public X get() {
				if (valueOp == null)
					synchronized (this) {
						if (valueOp == null)
							valueOp = Optional.ofNullable(supplier.get());
					}
				return valueOp.orElse(null);
			}
		};
	}
}
