package com.lfp.code.core.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import one.util.streamex.StreamEx;

public class XMLs {

	private XMLs() {
	};

	public static Document read(InputStream inputStream) throws DocumentException, IOException {
		try (inputStream) {
			SAXReader reader = new SAXReader();
			Document document = reader.read(inputStream);
			return document;
		}
	}

	public static void write(Document document, OutputStream outputStream) throws IOException {
		write(document, outputStream, null);
	}

	public static void write(Document document, OutputStream outputStream, OutputFormat format) throws IOException {
		try (outputStream) {
			XMLWriter writer = format == null ? new XMLWriter(outputStream) : new XMLWriter(outputStream, format);
			writer.write(document);
			writer.close();
		}
	}

	public static String toString(Document document) {
		if (document == null)
			return null;
		var baos = new ByteArrayOutputStream();
		try {
			write(document, baos, createPrettyPrintOutputFormat());
		} catch (IOException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
		var str = new String(baos.toByteArray(), StandardCharsets.UTF_8);
		str = str.trim();
		return str;
	}

	public static Element getOrAddElement(Document xmlDocument, String... paths) {
		return getOrAddElement(xmlDocument == null ? null : xmlDocument.getRootElement(), paths);
	}

	public static Element getOrAddElement(Element element, String... paths) {
		Objects.requireNonNull(element);
		var pathIter = Arrays.asList(paths).iterator();
		if (!pathIter.hasNext())
			return element;
		var next = pathIter.next();
		var child = element.element(next);
		if (child == null)
			child = element.addElement(next);
		return getOrAddElement(child, StreamEx.of(pathIter).toArray(String.class));
	}

	public static StreamEx<Element> streamElements(Document xmlDocument, String... paths) {
		return streamElements(xmlDocument == null ? null : xmlDocument.getRootElement(), paths);
	}

	public static StreamEx<Element> streamElements(Element element, String... paths) {
		if (element == null)
			return StreamEx.empty();
		var pathIter = Arrays.asList(paths).iterator();
		if (!pathIter.hasNext())
			return StreamEx.of(element);
		var next = pathIter.next();
		var elementStream = StreamEx.of(element.elementIterator());
		elementStream = elementStream.filter(v -> Objects.equals(v.getName(), next));
		var nextPaths = StreamEx.of(pathIter).toArray(String.class);
		if (nextPaths.length == 0)
			return elementStream;
		return elementStream.flatMap(v -> {
			return streamElements(v, nextPaths);
		});
	}

	public static StreamEx<String> streamText(Document xmlDocument, boolean notBlank, String... paths) {
		return streamText(xmlDocument == null ? null : xmlDocument.getRootElement(), notBlank, paths);
	}

	public static StreamEx<String> streamText(Element element, boolean notBlank, String... paths) {
		var stream = streamElements(element, paths).map(v -> v.getText());
		if (notBlank)
			stream = stream.filter(StringUtils::isNotBlank);
		return stream;
	}

	public static OutputFormat createPrettyPrintOutputFormat() {
		OutputFormat format = OutputFormat.createPrettyPrint();
		format.setNewLineAfterDeclaration(false);
		return format;
	}
}
