package com.lfp.code.core.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

public enum ConfigPrivateProperties {
    INSTANCE;

    private static final Class<?> THIS_CLASS = new Object() {
    }.getClass().getEnclosingClass();
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

    private Properties properties;

    public synchronized Properties getProperties() {
        if (properties != null)
            return properties;
        Properties prop = null;
        try (var stream = THIS_CLASS.getClassLoader()
                .getResourceAsStream("config-private/application.properties")) {
            prop = new Properties();
            prop.load(stream);
        } catch (Exception e) {
            prop = null;
            // suppress
        }
        if (prop == null) {
            File file = new File("src/main/resources/config-private/application.properties");
            try (var stream = new FileInputStream(file)) {
                prop = new Properties();
                prop.load(stream);
            } catch (IOException e) {
                prop = null;
                //suppress
            }
        }
        if (prop == null) {
            logger.warn("could not read properties");
            prop = new Properties();
        }
        this.properties = prop;
        return this.properties;
    }
}
