package com.lfp.code.core.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.lfp.code.core.mvn.PomReader;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class PomSorter {

	public static void sort(File rootDir, List<File> pomFiles) {
		StreamEx<File> pomFileStream = StreamEx.of(rootDir.listFiles())
				.map(f -> new File(f.getAbsolutePath() + "/pom.xml")).filter(File::exists).filter(f -> {
					if (Dirs.sameFile(f, rootDir))
						return false;
					return true;
				}).filter(f -> {
					for (File file : pomFiles) {
						if (Dirs.sameFile(f, file))
							return true;
					}
					return false;
				});
		StreamEx<PomReader> pomReaderStream = pomFileStream.map(PomReader::get).map(Optional::get);
		pomReaderStream = pomReaderStream.map(PomReader::getRootPomReader);
		pomReaderStream = pomReaderStream.distinct();
		Set<PomReader> pomReaders = pomReaderStream.toSet();
		Set<PomReader> lookup = StreamEx.of(pomFiles).map(PomReader::get).map(Optional::get).toSet();
		List<Map<PomReader, Integer>> trackers = new ArrayList<>();
		for (PomReader pr : lookup) {
			Map<PomReader, Integer> tracker = new HashMap<>();
			getAllPomReaders(tracker, 0, pr, pomReaders, new ArrayList<PomReader>());
			trackers.add(tracker);
		}
		StreamEx<PomReader> keyStream = StreamEx.of(trackers).flatCollection(Map::keySet).distinct();
		EntryStream<PomReader, Integer> entryStream = keyStream.mapToEntry(pr -> pr, pr -> {
			int largestIndex = 0;
			List<Entry<PomReader, Integer>> matchList = StreamEx.of(trackers).flatCollection(Map::entrySet)
					.filter(e -> pr.equals(e.getKey())).toList();
			for (Entry<PomReader, Integer> ent : matchList)
				if (ent.getValue() > largestIndex)
					largestIndex = ent.getValue();
			return largestIndex;
		});
		entryStream = entryStream.distinct();
		List<Entry<PomReader, Integer>> entryList = entryStream.toList();
		Comparator<Entry<PomReader, Integer>> comp = Comparator.comparing(v -> 0);
		comp = comp.thenComparing(e -> e.getValue() * -1);
		BiFunction<PomReader, PomReader, Boolean> parentOf = (pr1, pr2) -> {
			pr1 = pr1.getRootPomReader();
			pr2 = pr2.getRootPomReader();
			for (PomReader pr1Sub : pr1.streamAll())
				if (pr2.containsDependency(pr1Sub))
					return true;
			return false;
		};
		comp = comp.thenComparing(ent -> {
			PomReader pr = ent.getKey();
			if (pr.getFolderName().contains("lfp-websocket") || pr.getFolderName().contains("browserless"))
				System.out.println(pr);
			List<Entry<PomReader, Integer>> sameLevelEntries = StreamEx.of(entryList).filter(v -> v != ent)
					.filter(v -> v.getValue().equals(ent.getValue())).toList();
			int parentCount = 0;
			for (Entry<PomReader, Integer> sameLevelEntry : sameLevelEntries) {
				Boolean parent = parentOf.apply(pr, sameLevelEntry.getKey());
				if (parent)
					parentCount++;
			}
			return parentCount * -1;
		});
		comp = comp.thenComparing((e1, e2) -> {
			PomReader pr1 = e1.getKey().getRootPomReader();
			PomReader pr2 = e2.getKey().getRootPomReader();
			Boolean parent1 = parentOf.apply(pr1, pr2);
			Integer v1;
			if (parent1)
				v1 = 0;
			else
				v1 = 1;
			Boolean parent2 = parentOf.apply(pr2, pr1);
			Integer v2;
			if (parent2)
				v2 = 0;
			else
				v2 = 1;
			return v1.compareTo(v2);
		});
		Collections.sort(entryList, comp);
		entryStream = StreamEx.of(entryList).mapToEntry(Entry::getKey, Entry::getValue);
		AtomicLong indexTracker = new AtomicLong();
		List<File> ordered = entryStream.map(ent -> {
			long index = indexTracker.incrementAndGet();
			System.out.println(String.format("%s: %s [%s]", index, ent.getKey().getFolderName(), ent.getValue()));
			return ent.getKey();
		}).map(PomReader::getPomFile).toList();
		Collections.sort(pomFiles, Comparator.comparing(f -> {
			for (int i = 0; i < ordered.size(); i++) {
				File file = ordered.get(i);
				if (Dirs.sameFile(f, file))
					return i;
			}
			return Integer.MAX_VALUE;
		}));
	}

	private static void getAllPomReaders(Map<PomReader, Integer> tracker, int level, PomReader pomReader,
			Iterable<PomReader> otherPomReaders, List<PomReader> currentChain) {
		Function<PomReader, Boolean> trackerAdd = pr -> {
			AtomicBoolean mod = new AtomicBoolean();
			tracker.compute(pr, (k, v) -> {
				if (v != null && v >= level)
					return v;
				mod.set(true);
				return level;
			});
			return mod.get();
		};
		trackerAdd.apply(pomReader);
		for (PomReader otherPomReader : otherPomReaders) {
			if (!containsDependency(pomReader, otherPomReader))
				continue;
			if (!trackerAdd.apply(otherPomReader))
				continue;
			getAllPomReaders(tracker, level + 1, otherPomReader, otherPomReaders, currentChain);
		}
	}

	@SuppressWarnings("resource")
	private static boolean containsDependency(PomReader pomReader, PomReader otherPomReader) {
		pomReader = pomReader.getRootPomReader();
		otherPomReader = otherPomReader.getRootPomReader();
		for (PomReader pr : otherPomReader.streamAll()) {
			if (pomReader.containsDependency(pr))
				return true;
		}
		return false;
	}

}
