package com.lfp.code.core.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.maven.model.DistributionManagement;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.lfp.code.core.mvn.PomReader;

import one.util.streamex.StreamEx;

public class Mvn {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String SKIP_AUTO_UPDATES_PRE = "skip.auto.updates.";

	public static LinkedHashSet<File> doMavenIncrement(File pomFile)
			throws IOException, SAXException, ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, MavenInvocationException {
		LinkedHashSet<File> updatedPoms = new LinkedHashSet<File>();
		return doMavenIncrement(pomFile, updatedPoms);
	}

	public static LinkedHashSet<File> doMavenIncrement(File pomFile, LinkedHashSet<File> updatedPoms)
			throws IOException, SAXException, ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, MavenInvocationException {
		if (!updatedPoms.add(pomFile))
			return updatedPoms;
		Optional<PomReader> pomReaderOp = PomReader.get(pomFile);
		Validate.isTrue(pomReaderOp.isPresent(), "does not exists: " + pomFile.getAbsolutePath());
		backup(pomFile);
		PomReader pomReader = pomReaderOp.get();
		String curVersion = pomReader.getVersion();
		String newVersion = incrementVersion(curVersion);
		System.out.println(String.format("updating %s to version:%s", pomFile.getParentFile().getName(), newVersion));
		if (pomReader.setVersion(newVersion)) {
			pomReader.write();
			Validate.isTrue(newVersion.equals(pomReader.getVersion()), "version missmatch. %s - %s", newVersion,
					pomReader.getVersion());
		} else
			System.out.println("skipping version set:" + pomReader.getFolderName());
		updateOtherProjects(pomReader, updatedPoms);
		return updatedPoms;
	}

	public static void doMaven(File pomFile, String... goals) throws MavenInvocationException, IOException {
		File mvnHome = parseMavenHome();
		InvocationRequest request = new DefaultInvocationRequest();
		request.setThreads("2.0C");
		request.setPomFile(pomFile);
		Properties props = request.getProperties();
		props = props != null ? props : new Properties();
		props.setProperty("maven.test.skip", Objects.toString(true));
		props.setProperty("maven.wagon.provider.http", "httpclient");
		props.setProperty("maven.artifact.threads", Objects.toString(Runtime.getRuntime().availableProcessors() * 4));
		props.setProperty("http.tcp.nodelay", Objects.toString(false));
		request.setProperties(props);
		StreamEx<String> goalStream = goals == null ? StreamEx.empty() : StreamEx.of(Arrays.asList(goals));
		List<String> goalList = goalStream.filter(StringUtils::isNotBlank).toList();
		request.setGoals(goalList);
		request.setUpdateSnapshots(true);
		Invoker invoker = new DefaultInvoker();
		invoker.setMavenHome(mvnHome);
		InvocationResult result = invoker.execute(request);
		if (result.getExitCode() != 0) {
			throw new IOException("build of pom failed: " + pomFile);
		}
	}

	private static File parseMavenHome() throws IOException {
		String out = Process.execute("mvn -version");
		String lines[] = out.split("\\r?\\n");
		for (String line : lines) {
			if (line.toLowerCase().startsWith("maven home:".toLowerCase())) {
				String path = line.substring(line.indexOf(":") + 1);
				path = path.trim();
				File file = new File(path);
				if (file.exists()) {
					return file;
				}
			}
		}
		throw new IOException("could not parse maven home");
	}

	private static void updateOtherProjects(PomReader pomReader, LinkedHashSet<File> updatedPoms)
			throws IOException, SAXException, ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, MavenInvocationException {
		StreamEx<PomReader> pomReaderStream = streamSibilingPomReaders(pomReader);
		Set<File> incremented = new LinkedHashSet<>();
		for (PomReader root : pomReaderStream) {
			for (PomReader pr : root.streamAll()) {
				boolean mod = false;
				if (pr.setDependencyVersion(pomReader, pomReader.getVersion()))
					mod = true;
				if (mod) {
					incremented.add(root.getRootPomReader().getPomFile());
					pr.write();
				}
			}
		}
		for (File pomFile : incremented) {
			doMavenIncrement(pomFile, updatedPoms);
		}
	}

	private static StreamEx<PomReader> streamSibilingPomReaders(PomReader pomReader) {
		File pomFile = pomReader.getPomFile();
		StreamEx<File> fileStream = StreamEx.of(Dirs.getFoldersInDir(pomFile.getParentFile().getParentFile()))
				.filter(f -> {
					try {
						return !Objects.equals(pomFile.getParentFile().getCanonicalPath(), f.getCanonicalPath());
					} catch (IOException e) {
						throw java.lang.RuntimeException.class.isAssignableFrom(e.getClass())
								? java.lang.RuntimeException.class.cast(e)
								: new java.lang.RuntimeException(e);
					}
				}).map(f -> new File(f.getAbsolutePath() + "/pom.xml")).filter(f -> f.exists()).map(f -> {
					PomReader pr = PomReader.get(f).orElse(null);
					if (pr == null)
						return null;
					StreamEx<PomReader> prStream = pr.streamAll();
					List<File> pomFiles = prStream.map(v -> v.getPomFile()).toList();
					return pomFiles;
				}).nonNull().flatCollection(c -> c);
		fileStream = fileStream.distinct();
		return fileStream.map(f -> PomReader.get(f).orElse(null)).nonNull();
	}

	private static boolean skipUpdates(Document doc) {
		String key = SKIP_AUTO_UPDATES_PRE + "all";
		for (Node properties : toList(doc.getElementsByTagName("properties"))) {
			for (Node prop : toList(properties.getChildNodes())) {
				String nodeName = StringUtils.trim(prop.getNodeName());
				if (!StringUtils.equals(key, nodeName)) {
					continue;
				}
				String textContent = StringUtils.trim(prop.getTextContent());
				if (StringUtils.equalsIgnoreCase(Boolean.TRUE.toString(), textContent)) {
					return true;
				}
			}
		}
		return false;
	}

	private static boolean skipPropertyValueTrue(Document doc, Collection<String> groupIds,
			Collection<String> artifactIds) {
		for (String groupId : groupIds) {
			for (String artifactId : artifactIds) {
				String key = SKIP_AUTO_UPDATES_PRE + groupId + "." + artifactId;
				for (Node properties : toList(doc.getElementsByTagName("properties"))) {
					for (Node prop : toList(properties.getChildNodes())) {
						String nodeName = StringUtils.trim(prop.getNodeName());
						if (!StringUtils.equals(key, nodeName)) {
							continue;
						}
						String textContent = StringUtils.trim(prop.getTextContent());
						if (StringUtils.equalsIgnoreCase(Boolean.TRUE.toString(), textContent)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static List<Node> toList(NodeList deps) {
		List<Node> nodes = new ArrayList<Node>();
		for (int i = 0; deps != null && i < deps.getLength(); i++) {
			nodes.add(deps.item(i));
		}
		return nodes;
	}

	private static void backup(File pomFile) throws IOException {
		String newFilePath = pomFile.getParent() + "/temp/pom_" + new Date().getTime() + ".xml";
		FileUtils.copyFile(pomFile, new File(newFilePath));

	}

	private static String incrementVersion(String curVersion) {
		String post = "";
		if (curVersion.contains("-")) {
			int index = curVersion.indexOf("-");
			post = curVersion.substring(index);
			curVersion = curVersion.substring(0, index);
		}
		List<Integer> vers = new ArrayList<Integer>();
		for (String chunk : curVersion.split(Pattern.quote("."))) {
			int ver;
			try {
				ver = Integer.parseInt(chunk);
			} catch (NumberFormatException e) {
				continue;
			}
			vers.add(0, ver);
		}
		List<Integer> finalVers = new ArrayList<Integer>();
		boolean needsIncrement = true;
		Iterator<Integer> iter = vers.iterator();
		while (iter.hasNext()) {
			int ver = iter.next();
			if (needsIncrement) {
				ver++;
				needsIncrement = false;
				if (iter.hasNext() && ver >= 10) {
					needsIncrement = true;
					ver = 0;
				}
			}
			finalVers.add(ver);
		}
		String result = null;
		Collections.reverse(finalVers);
		for (Integer val : finalVers) {
			if (result == null) {
				result = "";
			} else {
				result += ".";
			}
			result += val;
		}
		return result + post;
	}

	public static List<File> getPomFilesMatchingFilters(List<String> argsAfter) throws IOException {
		File dirFile = Dirs.getProjectDir().getParentFile();
		List<File> folders = new ArrayList<File>();
		for (File file : Dirs.getFoldersInDir(dirFile)) {
			File pomFile = new File(file.getAbsolutePath() + "/pom.xml");
			if (pomFile.exists() && Dirs.validAndMatchesPattern(file, argsAfter)) {
				folders.add(pomFile);
			}
		}
		return folders;
	}

	public static boolean isDistributionManagement(File pomFile) {
		logger.info("checking distribution management:" + pomFile);
		return isDistributionManagement(PomReader.get(pomFile).get());
	}

	public static boolean isDistributionManagement(PomReader pomReader) {
		DistributionManagement distributionManagement = pomReader.getModel().getDistributionManagement();
		if (distributionManagement == null)
			return false;
		if (distributionManagement.getRepository() != null)
			return true;
		if (distributionManagement.getSnapshotRepository() != null)
			return true;
		return false;
	}

	@SuppressWarnings("resource")
	public static void orderPoms(File rootFolder, List<File> pomFiles) {
		PomSorter.sort(rootFolder, pomFiles);
	}

	public static void main(String[] args) {
		File dir = new File("C:\\Users\\reggi\\eclipse-workspace\\iplasso\\");
		StreamEx<File> pomStream = StreamEx.of(dir.listFiles()).map(f -> new File(f.getAbsolutePath() + "/pom.xml"))
				.filter(File::exists);
		Set<File> pomSet = pomStream.toCollection(() -> new LinkedHashSet<>());
		pomSet.clear();
		String blob = "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-okhttp\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-spring-base\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-browserless-service\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-minio-client\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\ipl-umarket-db\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-ssdb\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-browserless-client\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-aws\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-cdn-client\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\ipl-view-service\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-service-base\\pom.xml\n"
				+ "C:\\Users\\reggi\\eclipse-workspace\\iplasso\\lfp-websocket\\pom.xml";
		StreamEx.of(blob.split("\\r?\\n")).filter(StringUtils::isNotBlank).distinct().map(File::new)
				.forEach(f -> pomSet.add(f));
		List<File> poms = new ArrayList<>(pomSet);
		orderPoms(dir, poms);
		poms.forEach(p -> {
			System.out.println(p.getAbsolutePath());
		});
	}

}
