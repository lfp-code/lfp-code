package com.lfp.code.command;

import java.util.concurrent.Callable;

public interface Command<X> extends Callable<X> {
	
	public static Command<Void> create(String name, Runnable command) {
		return create(name, () -> {
			command.run();
			return null;
		});
	}

	public static <X> Command<X> create(String name, Callable<X> command) {
		return new Command<X>() {

			@Override
			public X call() throws Exception {
				return command.call();
			}

			@Override
			public String getName() {
				return name;
			}
		};
	}

	String getName();

}
