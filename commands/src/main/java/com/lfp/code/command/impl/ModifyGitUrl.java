package com.lfp.code.command.impl;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.lfp.code.command.Command;
import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.Dirs;

import one.util.streamex.StreamEx;

public class ModifyGitUrl implements Command<File> {
	private static final String URL_TOKEN = "url = ";

	@Override
	public File call() throws Exception {
		var folderPath = ConsoleReader.INSTANCE.readConsoleLine("Enter project folder path (ex: 'lfp-joe'):");
		var folder = new File(folderPath);
		if (!folder.exists()) {
			File rootDir = Dirs.getProjectDir().getParentFile();
			folder = new File(rootDir, folderPath);
		}
		File gitConfig = new File(folder, ".git" + File.separator + "config");
		Validate.isTrue(folder.exists(), "unable to find git config:%s", gitConfig.getAbsolutePath());
		var url = ConsoleReader.INSTANCE
				.readConsoleLine("Enter a new git url (ex: 'git@bitbucket.org:regbo/lfp-joe.git'):");
		List<String> lines = new ArrayList<>();
		try (var liter = FileUtils.lineIterator(gitConfig, StandardCharsets.UTF_8.name());) {
			boolean replaced = false;
			while (liter.hasNext()) {
				var line = liter.next();
				if (!replaced) {
					int index = StringUtils.indexOf(line, URL_TOKEN);
					if (index >= 0) {
						line = line.substring(0, index + URL_TOKEN.length());
						line += url;
						replaced = true;
					}
				}
				lines.add(line);
			}
			Validate.isTrue(replaced, "unable to replace url:%s", gitConfig.getAbsolutePath());
		}
		var out = StreamEx.of(lines).joining("\n");
		FileUtils.write(gitConfig, out, StandardCharsets.UTF_8);
		System.out.println("success");
		return gitConfig;
	}

	@Override
	public String getName() {
		return "Modify git URL";
	}

	public static void main(String[] args) throws Exception {
		new ModifyGitUrl().call();
	}

}
