package com.lfp.code.command.impl;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.lfp.code.command.Command;
import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.Dirs;
import com.lfp.code.core.services.Mvn;

import one.util.streamex.StreamEx;

public class VaadinCleanCompile implements Command<Void> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public Void call() throws Exception {
		var folderPath = ConsoleReader.INSTANCE.readConsoleLine("Enter project folder path (ex: 'lfp-joe'):");
		var folder = new File(folderPath);
		if (!folder.exists()) {
			File rootDir = Dirs.getProjectDir().getParentFile();
			folder = new File(rootDir, folderPath);
		}
		Validate.isTrue(folder.exists());
		var installInput = ConsoleReader.INSTANCE.readConsoleLine("Perform Maven install? [y, (n/blank)]:", true);
		boolean install = false;
		for (var check : List.of("y", "yes", Boolean.TRUE.toString()))
			if (StringUtils.equalsIgnoreCase(installInput, check)) {
				install = true;
				break;
			}
		for (var prefix : List.of("", "target")) {
			if (StringUtils.isNotBlank(prefix))
				prefix = prefix + File.separator;
			var file = new File(folder, prefix + "ws-updated");
			if (FileUtils.deleteQuietly(file))
				logger.info("deleted:" + file.getAbsolutePath());
		}
		{
			var file = new File(folder,
					StreamEx.of("src", "main", "resources", "VAADIN", "widgetsets").joining(File.separator));
			if (file.exists()) {
				FileUtils.deleteDirectory(file);
				logger.info("deleted:" + file.getAbsolutePath());
			}
		}
		{
			var file = new File(folder, StreamEx.of("target").joining(File.separator));
			if (file.exists()) {
				FileUtils.deleteDirectory(file);
				logger.info("deleted:" + file.getAbsolutePath());
			}
		}

		var projectDir = Dirs.getProjectDir(folder);
		if (!Dirs.sameFile(folder, projectDir)) {
			var pom = new File(projectDir, "pom.xml");
			if (pom.exists())
				Mvn.doMaven(pom, "-pl", String.format("!%s", folder.getName()), "compile", "install");
		}
		var goalStream = StreamEx.of("clean", "vaadin:clean", "vaadin:update-widgetset", "vaadin:update-theme",
				"vaadin:compile-theme", "vaadin:compile");
		if (install)
			goalStream = goalStream.append("install");
		Mvn.doMaven(new File(folder, "pom.xml"), goalStream.toArray(String.class));
		System.out.println("If possible also run widgetset compile in eclipse");
		return null;
	}

	@Override
	public String getName() {
		return "Vaadin clean and compile";
	}

}
