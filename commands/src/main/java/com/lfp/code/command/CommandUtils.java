package com.lfp.code.command;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import one.util.streamex.StreamEx;

public class CommandUtils {

	public static String removeSubModuleOverlap(String projectPath, String subModuleName) {
		Validate.notBlank(projectPath);
		subModuleName = StringUtils.trimToNull(subModuleName);
		if (subModuleName == null)
			return subModuleName;
		if (subModuleName.startsWith(projectPath))
			subModuleName = subModuleName.substring(projectPath.length());
		else {
			var projectPathLast = StreamEx.of(projectPath.split("\\W")).filter(StringUtils::isNotBlank)
					.reduce((f, s) -> s).get();
			if (subModuleName.startsWith(projectPathLast))
				subModuleName = subModuleName.substring(projectPathLast.length());
		}
		while (StringUtils.isNotBlank(subModuleName)) {
			var firstChar = subModuleName.charAt(0);
			if (Character.isAlphabetic(firstChar))
				break;
			if (Character.isDigit(firstChar))
				break;
			subModuleName = subModuleName.substring(1);
		}
		Validate.notBlank(subModuleName);
		return subModuleName;
	}

	public static void main(String[] args) {
		var result = removeSubModuleOverlap("lfp-proxy-routing", "lfp-proxy-routing-gateway");
		System.out.println(result);
	}
}
