package com.lfp.code.command.impl;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.maven.shared.invoker.MavenInvocationException;

import com.github.jknack.handlebars.Handlebars;
import com.lfp.code.command.Command;
import com.lfp.code.command.CommandUtils;
import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.Dirs;
import com.lfp.code.core.services.Mvn;

public class CreateMultiModuleProject implements Command<File> {
	@Override
	public String getName() {
		return "Create multi module project";
	}

	@Override
	public File call() throws IOException, MavenInvocationException {
		Set<Closeable> onClose = ConcurrentHashMap.newKeySet();
		try {
			return callInternal(onClose);
		} finally {
			onClose.forEach(IOUtils::closeQuietly);
		}

	}

	private File callInternal(Set<Closeable> onClose) throws IOException, MavenInvocationException {
		File rootDir = Dirs.getProjectDir().getParentFile();
		String joeVersion;
		{
			var joePom = new File(rootDir.getAbsolutePath() + "/lfp-joe/pom.xml");
			var contents = FileUtils.readFileToString(joePom, StandardCharsets.UTF_8);
			var pattern = Pattern.compile("<revision>.*?</revision>");
			var matcher = pattern.matcher(contents);
			matcher.find();
			var xml = matcher.group();
			joeVersion = StringUtils.substringAfter(StringUtils.substringBeforeLast(xml, "</"), ">");
		}
		var groupIdentifier = ConsoleReader.INSTANCE.readConsoleLine("Enter group identifier (ex: 'lfp'):");
		Validate.isTrue(StringUtils.isNotBlank(groupIdentifier), "invalid group identifier");
		var projectName = ConsoleReader.INSTANCE.readConsoleLine("Enter project name:");
		projectName = CommandUtils.removeSubModuleOverlap(groupIdentifier, projectName);
		Validate.isTrue(StringUtils.isNotBlank(projectName), "invalid project name");
		File finalDir = new File(rootDir, String.format("%s-%s", groupIdentifier, projectName));
		if (finalDir.exists()) {
			var files = finalDir.listFiles();
			boolean validFiles = true;
			for (var f : files) {
				if (".git".equals(f.getName()))
					continue;
				if (".gitignore".equals(f.getName()))
					continue;
				validFiles = false;
				break;
			}
			Validate.isTrue(validFiles, "project location exists:%s", finalDir.getAbsolutePath());
		}
		File tempDir;
		{
			var templateDir = new File("src/main/resources/templates/module-project");
			Validate.isTrue(templateDir.exists(), "templateDir not found");
			tempDir = new File(String.format("temp/module-project-%s-%s-%s", groupIdentifier, projectName,
					UUID.randomUUID().toString()));
			onClose.add(() -> FileUtils.deleteQuietly(tempDir));
			FileUtils.copyDirectory(templateDir, tempDir, false);
		}
		var handlebars = new Handlebars();
		for (var f : Dirs.streamFiles(tempDir).filter(f -> f.getName().equals("pom.xml"))) {
			var contents = FileUtils.readFileToString(f, StandardCharsets.UTF_8);
			var updatedContents = handlebars.compileInline(contents).apply(Map.of("PROJECT_NAME", projectName,
					"JOE_VERSION", joeVersion, "GROUP_IDENTIFIER", groupIdentifier));
			FileUtils.writeStringToFile(f, updatedContents, StandardCharsets.UTF_8);
			var parentDir = f.getParentFile();
			if (Objects.equals(parentDir, tempDir))
				continue;
			File srcDir = new File(parentDir,
					String.format("src/main/java/com/%s/%s/%s", groupIdentifier, projectName, parentDir.getName()));
			FileUtils.forceMkdir(srcDir);
		}
		FileUtils.copyDirectory(tempDir, finalDir);
		Mvn.doMaven(new File(finalDir, "pom.xml"), "clean", "compile", "install");
		return finalDir;
	}

	public static void main(String[] args) throws Exception {
		var templateDir = new File("src/main/resources/templates/module-project");
		Arrays.asList(templateDir.listFiles()).forEach(f -> {
			System.out.println(f.getAbsolutePath());
		});
	}
}
