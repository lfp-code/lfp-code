package com.lfp.code.command.impl;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import com.lfp.code.command.Command;
import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.Dirs;
import com.lfp.code.core.services.Pools;

public abstract class GitCommand implements Command<Long> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public Long call() throws Exception {
		File folder = Dirs.getProjectDir().getParentFile();
		var filter = ConsoleReader.INSTANCE.readConsoleLine("Add folder filters (ex: 'lfp-cool-*'):", true);
		var dirStream = Dirs.streamFiles(folder, filter);
		dirStream = dirStream.filter(File::isDirectory);
		dirStream = dirStream.filter(v -> {
			var f = new File(v, ".git");
			return f.exists();
		});
		var pool = Pools.limited();
		var futures = dirStream.map(dir -> {
			logger.info("requested:{}", dir.getName());
			var future = CompletableFuture.supplyAsync(() -> {
				try {
					processDir(dir);
				} catch (IOException | GitAPIException e) {
					throw new CompletionException(e);
				}
				return null;
			}, pool);
			future.whenComplete((v, t) -> {
				if (t != null)
					logger.warn("error:{}", dir.getName(), t);
				else
					logger.info("completed:{}", dir.getName());
			});
			return future;
		}).toArray(CompletableFuture.class);
		var count = CompletableFuture.allOf(futures).thenApply(nil -> futures.length).join();
		return (long) count;
	}

	protected void processDir(File dir) throws IOException, GitAPIException {
		Repository repo = new FileRepositoryBuilder().setGitDir(new File(dir, ".git")).build();
		logger.info("starting: " + dir.getName());
		try (Git git = new Git(repo);) {
			process(dir, git);
			return;
		}
	}

	protected abstract void process(File dir, Git git) throws GitAPIException;

	public static class AddCommitPush extends GitCommand {

		@Override
		public String getName() {
			return "Git pull, add, commit, and push";
		}

		@Override
		protected void process(File dir, Git git) throws GitAPIException {
			Date now = new Date();
			var diffEntries = git.diff().call();
			if (diffEntries.isEmpty())
				return;
			git.fetch().call();
			git.pull().call();
			// add removed and modified file
			git.add().setUpdate(true).addFilepattern(".").call();
			// add new and modified file
			git.add().addFilepattern(".").call();
			git.commit().setMessage("Auto JGit Commit " + DateFormat.getDateInstance(DateFormat.SHORT).format(now)
					+ " - " + DateFormat.getTimeInstance(DateFormat.SHORT).format(now)).call();
			git.push().call();

		}
	}

	public static class Pull extends GitCommand {

		@Override
		public String getName() {
			return "Git pull";
		}

		@Override
		protected void process(File dir, Git git) throws GitAPIException {
			git.fetch().call();
			git.pull().call();
		}
	}

}
