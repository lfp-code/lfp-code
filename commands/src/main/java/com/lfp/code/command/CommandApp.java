package com.lfp.code.command;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.EmailLogger;

import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ScanResult;
import one.util.streamex.StreamEx;

public class CommandApp {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	private List<Command<?>> buildCommands() {
		Set<String> names = new HashSet<>();
		try (ScanResult scanResult = new ClassGraph().enableClassInfo().scan()) { // Start the scan
			for (ClassInfo classInfo : scanResult.getClassesImplementing(Command.class.getName())) {
				names.add(classInfo.getName());
			}
		}
		var classTypes = StreamEx.of(names).map(t -> {
			try {
				return Class.forName(t);
			} catch (ClassNotFoundException e1) {
				throw (((Object) e1) instanceof java.lang.RuntimeException) ? java.lang.RuntimeException.class.cast(e1)
						: new RuntimeException(e1);
			}
		}).toSet();
		classTypes.removeIf(v -> {
			if (v.isInterface())
				return true;
			if (Modifier.isAbstract(v.getModifiers()))
				return true;
			return false;
		});
		List<Command<?>> cmds = new ArrayList<>();
		classTypes.forEach(v -> {
			try {
				cmds.add((Command<?>) v.newInstance());
			} catch (InstantiationException | IllegalAccessException e) {
				throw (((Object) e) instanceof java.lang.RuntimeException) ? java.lang.RuntimeException.class.cast(e)
						: new RuntimeException(e);
			}
		});
		Collections.sort(cmds, Comparator.comparing(v -> StringUtils.lowerCase(v.getName())));
		return cmds;
	}

	private Command<?> selectCommand() {
		List<Command<?>> commands = buildCommands();
		String out = "\n************************\nSelect Command:";
		int index = -1;
		for (var cmd : commands) {
			index++;
			out += String.format("\n\t(%s) - %s", index + 1, cmd.getName());
		}
		var commandNumber = Integer.valueOf(ConsoleReader.INSTANCE.readConsoleLine(out));
		var command = commands.get(commandNumber - 1);
		return command;
	}

	public static void main(String[] args) throws IOException {
		var outFile = new File(String.format("logs/build-v3-%s.log", UUID.randomUUID().toString()));
		outFile.getParentFile().mkdirs();
		PrintStream originalSystemOut = System.out;
		PrintStream originalSystemErr = System.err;
		FileOutputStream fileOut = new FileOutputStream(outFile);
		System.setOut(new PrintStream(new TeeOutputStream(originalSystemOut, fileOut), true));
		System.setErr(new PrintStream(new TeeOutputStream(originalSystemErr, fileOut), true));
		Throwable error = null;
		Command<?> command = null;
		try {
			var app = CommandApp.class.newInstance();
			command = app.selectCommand();
			Objects.requireNonNull(command);
			command.call();
		} catch (Throwable t) {
			t.printStackTrace();
			error = t;
		} finally {
			System.setOut(originalSystemOut);
			System.setErr(originalSystemErr);
			fileOut.close();
		}
		var commandName = command == null ? null : command.getName();
		File errorFile;
		String subject;
		String body;
		if (error != null) {
			errorFile = new File(String.format("temp/error-%s.txt", UUID.randomUUID().toString()));
			FileUtils.writeStringToFile(errorFile, ExceptionUtils.getStackTrace(error), StandardCharsets.UTF_8);
			subject = String.format("Command failed - %s", commandName);
			body = String.format("<h3>%s</h3><p>%s</p>", commandName,
					"command failed, log and stack trace are attached");
		} else {
			errorFile = null;
			subject = String.format("Command success - %s", commandName);
			body = String.format("<h3>%s</h3><p>%s</p>", commandName,
					"command completed successfully, log is attached");
		}
		try {
			EmailLogger.INSTANCE.sendMessage("reggie.pierce@iplasso.com", subject, body, outFile, errorFile);
		} finally {
			Arrays.asList(outFile, errorFile).forEach(FileUtils::deleteQuietly);
		}
		System.err.println("done - summary emailed");
	}
}
