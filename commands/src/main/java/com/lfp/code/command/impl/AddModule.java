package com.lfp.code.command.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.github.jknack.handlebars.Handlebars;
import com.lfp.code.command.Command;
import com.lfp.code.core.mvn.PomReader;
import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.Dirs;
import com.lfp.code.core.services.XMLs;

public class AddModule implements Command<File> {

	@Override
	public File call() throws Exception {
		var folderPath = ConsoleReader.INSTANCE.readConsoleLine("Enter project folder path (ex: 'lfp-joe'):");
		var folder = new File(folderPath);
		if (!folder.exists()) {
			File rootDir = Dirs.getProjectDir().getParentFile();
			folder = new File(rootDir, folderPath);
		}
		var pomReader = PomReader.get(new File(folder, "pom.xml")).orElse(null);
		Validate.isTrue(pomReader != null, "unable to find pom in folder with path:" + folderPath);
		var moduleName = ConsoleReader.INSTANCE.readConsoleLine("Enter new module name (ex: 'api'):");
		var exists = false;
		for (var module : pomReader.getModules()) {
			if (module.getFolderName().equals(moduleName)) {
				exists = true;
				break;
			}
		}
		Validate.isTrue(!exists, "module name exists:" + moduleName);
		var moduleDir = new File(folder, moduleName);
		var templateDir = new File("src/main/resources/templates/module-project-add/");
		FileUtils.copyDirectory(templateDir, moduleDir);
		var modulePom = new File(moduleDir, "pom.xml");
		var handlebars = new Handlebars();
		var contents = FileUtils.readFileToString(modulePom, StandardCharsets.UTF_8);
		String projectName = StringUtils.substringBeforeLast(pomReader.getArtifactId(), "-parent");
		var updatedContents = handlebars.compileInline(contents).apply(Map.of("GROUP_IDENTIFIER",
				pomReader.getGroupId(), "PROJECT_NAME", projectName, "MODULE_NAME", moduleName));
		FileUtils.writeStringToFile(modulePom, updatedContents, StandardCharsets.UTF_8);
		File packageFolder;
		{
			List<String> packageParts = new ArrayList<>(List.of("src", "main", "java"));
			packageParts.addAll(Arrays.asList(pomReader.getGroupId().split(Pattern.quote("."))));
			packageParts.add(projectName);
			packageParts.addAll(Arrays.asList(moduleName.split(Pattern.quote("-"))));
			var packagePath = packageParts.stream().collect(Collectors.joining(File.separator));
			packagePath = moduleDir.getAbsolutePath() + File.separator + packagePath;
			packageFolder = new File(packagePath);
		}
		FileUtils.forceMkdir(packageFolder);
		var parentPomXml = FileUtils.readFileToString(pomReader.getPomFile(), StandardCharsets.UTF_8);
		parentPomXml = parentPomXml.replace("</modules>", String.format("<module>%s</module></modules>", moduleName));
		var document = XMLs.read(new ByteArrayInputStream(parentPomXml.getBytes()));
		XMLs.write(document, new FileOutputStream(pomReader.getPomFile()), XMLs.createPrettyPrintOutputFormat());
		return moduleDir;
	}

	@Override
	public String getName() {
		return "Add module";
	}

	public static void main(String[] args) throws Exception {
		new AddModule().call();
	}

}
