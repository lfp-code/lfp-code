package com.lfp.code.command.impl;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.lfp.code.command.Command;
import com.lfp.code.command.CommandUtils;
import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.Dirs;

import one.util.streamex.StreamEx;

public class CreateResourceFolder implements Command<File> {

	@Override
	public File call() throws Exception {
		var folderPath = ConsoleReader.INSTANCE.readConsoleLine("Enter project folder path (ex: 'lfp-joe'):");
		var folder = new File(folderPath);
		if (!folder.exists()) {
			File rootDir = Dirs.getProjectDir().getParentFile();
			folder = new File(rootDir, folderPath);
		}
		var subModuleName = ConsoleReader.INSTANCE.readConsoleLine("Enter an optional sub module name (ex: 'api'):",
				true);
		subModuleName = CommandUtils.removeSubModuleOverlap(folderPath, subModuleName);
		if (StringUtils.isNotBlank(subModuleName))
			folder = new File(folder, subModuleName);
		Validate.isTrue(folder.exists(), "unable to find project folder with path:%s and subModule:%s", folderPath,
				subModuleName);
		var path = ConsoleReader.INSTANCE
				.readConsoleLine("Enter a resource folder path (ex: 'com/lfp/x' or 'com.lfp.x'):");
		path = StringUtils.stripStart(path, "\\.");
		path = StringUtils.stripEnd(path, "\\.");
		var chunks = path.split("\\.|\\\\");
		var chunkStream = StreamEx.of(chunks).prepend(Arrays.asList("src", "main", "resources"));
		chunkStream = chunkStream.filter(StringUtils::isNotBlank);
		var resourceFolder = new File(folder, chunkStream.joining(File.separator));
		Validate.isTrue(!resourceFolder.exists(), "resourceFolder already exists:%s", resourceFolder.getAbsolutePath());
		resourceFolder.mkdirs();
		Validate.isTrue(resourceFolder.exists() && resourceFolder.isDirectory());
		System.err.println("Created resource folder:\n" + resourceFolder.getAbsolutePath());
		return resourceFolder;
	}

	@Override
	public String getName() {
		return "Create resource folder";
	}

	public static void main(String[] args) throws Exception {
		new CreateResourceFolder().call();
	}

}
