package com.lfp.code.command.impl;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import com.lfp.code.command.Command;
import com.lfp.code.core.services.ConsoleReader;
import com.lfp.code.core.services.Dirs;

public class ArchiveProjects implements Command<Long> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);

	@Override
	public Long call() throws Exception {
		var lineStream = ConsoleReader.INSTANCE.streamConsoleLines("Enter projects to bulk exclude (ex: 'lfp-joe'):");
		lineStream = lineStream.filter(StringUtils::isNotBlank);
		var skipFolders = lineStream.map(line -> {
			var folder = new File(line);
			if (!folder.exists() || !folder.isDirectory()) {
				File rootDir = Dirs.getProjectDir().getParentFile();
				folder = new File(rootDir, line);
			}
			Validate.isTrue(folder.exists() && folder.isDirectory(), "folder does not exist:%s", line);
			return folder;
		}).toList();
		File rootDir = Dirs.getProjectDir().getParentFile();
		File archiveDir = new File(rootDir, ".archive");
		var total = 0l;
		for (var file : rootDir.listFiles()) {
			if (!file.isDirectory())
				continue;
			if (file.equals(archiveDir))
				continue;
			if (skipFolders.contains(file))
				continue;
			if (StringUtils.startsWithIgnoreCase(file.getName(), "."))
				continue;
			var proceedInput = ConsoleReader.INSTANCE
					.readConsoleLine(String.format("Archive project '%s'? (yes, y, or true)", file.getName()), true);
			boolean proceed;
			if (StringUtils.equalsIgnoreCase(Boolean.TRUE.toString(), proceedInput))
				proceed = true;
			else if (StringUtils.equalsIgnoreCase("yes", proceedInput))
				proceed = true;
			else if (StringUtils.equalsIgnoreCase("y", proceedInput))
				proceed = true;
			else
				proceed = false;
			if (!proceed) {
				logger.info("skipping:{}", file.getAbsolutePath());
				continue;
			}
			var moveTo = new File(archiveDir, file.getName());
			Validate.isTrue(!moveTo.exists(), "archive directory exists:%s", archiveDir.getAbsolutePath());
			FileUtils.moveDirectory(file, moveTo);
			logger.info("archived:{}", file.getAbsolutePath());
			total++;
		}
		return total;
	}

	@Override
	public String getName() {
		return "Archive projects";
	}

	public static void main(String[] args) throws Exception {
		new ArchiveProjects().call();
	}

}
