#REPO_SITE_BASE="tutum.co/reggiepierce"
REPO_SITE_BASE="reggiepierce"
MVN_COMMAND="mvn clean package -U"
repo=$1
if [ -z "$repo" ]
  then
  repo=${PWD##*/}
  echo "repo not specified, using $repo"
fi
rm docker/app-*.jar
eval $MVN_COMMAND
mv target/app-*.jar docker/
cd docker/
now=$(date +%s%3N)
nowTag="$REPO_SITE_BASE/$repo:$now"
latestTag="$REPO_SITE_BASE/$repo:latest"
docker build --pull -t $nowTag -t $latestTag .
docker push $nowTag
echo $nowTag
docker push "$latestTag"
echo $latestTag