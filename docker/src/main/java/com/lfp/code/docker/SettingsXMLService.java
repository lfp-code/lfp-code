package com.lfp.code.docker;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;

import com.lfp.code.core.services.Resources;
import com.lfp.code.core.services.XMLs;

import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

public class SettingsXMLService {

	private static final String DELIMITER = "_";

	private static final String ENV_VARIABLE_PREFIX = "MAVEN_SETTINGS_XML";

	private static final File USER_SETTINGS_XML = new File(FileUtils.getUserDirectory(),
			StreamEx.of(".m2", "settings.xml").joining(File.separator));

	private SettingsXMLService() {
	}

	public static Document buildDocument() throws DocumentException, IOException {
		Document xmlDocument = XMLs.read(Resources.get("templates/settings.xml"));
		streamEnvPaths().forEach(ent -> {
			append(xmlDocument.getRootElement(), ent.getKey(), ent.getValue());
		});
		System.out.println(XMLs.toString(xmlDocument));
		return xmlDocument;
	}

	public static void write(File outFile) throws DocumentException, IOException {
		var document = buildDocument();
		XMLs.write(document, new FileOutputStream(outFile));
	}

	public static EntryStream<String, String> environmentEntryStream()
			throws FileNotFoundException, DocumentException, IOException {
		return environmentEntryStream(XMLs.read(new FileInputStream(USER_SETTINGS_XML)));
	}

	public static EntryStream<String, String> environmentEntryStream(String xml) {
		try {
			return environmentEntryStream(XMLs.read(new ByteArrayInputStream(xml.getBytes())));
		} catch (DocumentException | IOException e) {
			throw RuntimeException.class.isInstance(e) ? RuntimeException.class.cast(e) : new RuntimeException(e);
		}
	}

	public static EntryStream<String, String> environmentEntryStream(Document document) {
		var rootElement = Optional.ofNullable(document).map(Document::getRootElement).orElse(null);
		var estream = environmentEntryStream(List.of(), rootElement);
		Comparator<Entry<String, String>> sorter = (v1, v2) -> 0;
		sorter = sorter.thenComparing(ent -> {
			var key = ent.getKey();
			key = StringUtils.lowerCase(key);
			key = StringUtils.substringBeforeLast(key, "]");
			return key;
		});
		sorter = sorter.thenComparing(ent -> {
			return StringUtils.endsWithIgnoreCase(ent.getKey(), DELIMITER + "id") ? 0 : 1;
		});

		sorter = sorter.thenComparing(ent -> {
			return ent.getKey();
		});
		return estream.sorted(sorter);
	}

	private static void append(Element element, List<String> path, String value) {
		if (element == null)
			return;
		if (path.isEmpty()) {
			element.setText(value);
			return;
		}
		var nextName = path.get(0);
		path = path.subList(1, path.size());
		var index = path.isEmpty() ? -1 : decodeIndex(path.get(0)).orElse(-1);
		if (index >= 0)
			path = path.subList(1, path.size());
		else
			index = 0;
		var childIterator = StreamEx.of(element.elementIterator()).filter(v -> Objects.equals(v.getName(), nextName))
				.iterator();
		Element nextElement = null;
		for (int i = 0; i < index + 1; i++) {
			if (childIterator.hasNext())
				nextElement = childIterator.next();
			else
				nextElement = element.addElement(nextName);
		}
		append(nextElement, path, value);
	}

	private static EntryStream<String, String> environmentEntryStream(List<String> path, Element element) {
		if (element == null)
			return EntryStream.empty();
		var text = element.getText();
		if (StringUtils.isNotBlank(text))
			return EntryStream.of(StreamEx.of(path).prepend(ENV_VARIABLE_PREFIX).joining(DELIMITER), text);
		EntryStream<String, String> result = EntryStream.empty();
		var nameToElements = StreamEx.of(element.elementIterator()).mapToEntry(v -> v.getName()).invert()
				.grouping(LinkedHashMap::new);
		for (var ent : nameToElements.entrySet()) {
			var name = ent.getKey();
			var elements = ent.getValue();
			var nextPath = StreamEx.of(path).append(name).toList();
			if (elements.size() == 1) {
				var append = environmentEntryStream(nextPath, elements.iterator().next());
				result = result.append(append);
			} else
				for (int i = 0; i < elements.size(); i++) {
					var append = environmentEntryStream(StreamEx.of(nextPath).append(encodeIndex(i)).toList(),
							elements.get(i));
					result = result.append(append);
				}
		}
		return result;
	}

	private static EntryStream<String, String> streamEnv() {
		List<Map<? extends Object, ? extends Object>> maps = Arrays.asList(System.getenv(), System.getProperties());
		EntryStream<String, String> estream = EntryStream.empty();
		for (var map : maps) {
			if (map == null)
				continue;
			var append = EntryStream.of(map).nonNullKeys().nonNullValues().mapKeys(Objects::toString)
					.mapValues(Objects::toString);
			append = append.filterKeys(StringUtils::isNotBlank);
			append = append.filterValues(StringUtils::isNotBlank);
			estream = estream.append(append);
		}
		return estream;
	}

	private static EntryStream<List<String>, String> streamEnvPaths() {
		var prefix = ENV_VARIABLE_PREFIX + DELIMITER;
		return streamEnv().mapKeys(key -> {
			if (!StringUtils.startsWithIgnoreCase(key, prefix))
				return null;
			key = key.substring(prefix.length());
			return List.of(key.split(Pattern.quote(DELIMITER)));
		}).nonNullKeys();
	}

	private static String encodeIndex(int index) {
		return String.format("[%s]", index);
	}

	private static OptionalInt decodeIndex(String input) {
		if (!StringUtils.startsWith(input, "["))
			return OptionalInt.empty();
		if (!StringUtils.endsWith(input, "]"))
			return OptionalInt.empty();
		input = input.substring(1, input.length() - 1);
		if (!StringUtils.isNumeric(input))
			return OptionalInt.empty();
		return OptionalInt.of(Integer.valueOf(input));
	}

	public static void main(String[] args) throws IOException, DocumentException {
		var entries = SettingsXMLService.environmentEntryStream().toList();
		entries.forEach(ent -> {
			// System.out.println(ent.getKey() + "=" + ent.getValue());
		});
		entries.forEach(ent -> {
			var out = String.format("System.setProperty(\"%s\", \"%s\");", ent.getKey(), ent.getValue());
			System.out.println(out);
		});
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[0]_username", "reggie.pierce");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[0]_password",
				"{TTX0ipH21Y0H+dxax49OqdqFM0V0AuSqUejBVwjHwx5CQ96KvwXgCbEvy+Xmp4ea}");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[0]_id", "central");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[1]_username", "reggie.pierce");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[1]_password",
				"{TTX0ipH21Y0H+dxax49OqdqFM0V0AuSqUejBVwjHwx5CQ96KvwXgCbEvy+Xmp4ea}");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[1]_id", "snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[2]_username", "reggie.pierce@iplasso.com");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[2]_password",
				"{5bPSJl2sN9EHnyzK2E+4vjqqWE3HeBQqz2ft4ZJ8NN7j/Cwf67nkqP4dNRDp6bXbK2jtp81SRCC0kREInrKDfw==}");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[2]_id", "nexus.lfpcode.com-releases");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[3]_username", "reggie.pierce@iplasso.com");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[3]_password",
				"{5bPSJl2sN9EHnyzK2E+4vjqqWE3HeBQqz2ft4ZJ8NN7j/Cwf67nkqP4dNRDp6bXbK2jtp81SRCC0kREInrKDfw==}");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[3]_id", "nexus.lfpcode.com-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[4]_username", "reggie.pierce@iplasso.com");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[4]_password",
				"{zGco75cHiyIH2TjvawW09BWvS22isSVvCO4HMoeGFcesmhhKj8cM1aFSXLN1Ki/M5pZHuNk6uiqP10og6Nk2fA==}");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[4]_id", "nexus.lasso.tm-releases");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[5]_username", "reggie.pierce@iplasso.com");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[5]_password",
				"{zGco75cHiyIH2TjvawW09BWvS22isSVvCO4HMoeGFcesmhhKj8cM1aFSXLN1Ki/M5pZHuNk6uiqP10og6Nk2fA==}");
		System.setProperty("MAVEN_SETTINGS_XML_servers_server_[5]_id", "nexus.lasso.tm-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_id", "artifactory");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_repositories_repository_[0]_snapshots_enabled",
				"false");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_repositories_repository_[0]_id", "central");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_repositories_repository_[0]_name", "libs-release");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_repositories_repository_[0]_url",
				"https://lib.iplasso.com/artifactory/libs-release");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_repositories_repository_[1]_id", "snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_repositories_repository_[1]_name", "libs-snapshot");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_repositories_repository_[1]_url",
				"https://lib.iplasso.com/artifactory/libs-snapshot");
		System.setProperty(
				"MAVEN_SETTINGS_XML_profiles_profile_[0]_pluginRepositories_pluginRepository_[0]_snapshots_enabled",
				"false");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_pluginRepositories_pluginRepository_[0]_id",
				"central");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_pluginRepositories_pluginRepository_[0]_name",
				"libs-release");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_pluginRepositories_pluginRepository_[0]_url",
				"https://lib.iplasso.com/artifactory/libs-release");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_pluginRepositories_pluginRepository_[1]_id",
				"snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_pluginRepositories_pluginRepository_[1]_name",
				"libs-snapshot");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_pluginRepositories_pluginRepository_[1]_url",
				"https://lib.iplasso.com/artifactory/libs-snapshot");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_properties_altSnapshotDeploymentRepository",
				"snapshots::default::https://lib.iplasso.com/artifactory/libs-snapshot");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[0]_properties_altReleaseDeploymentRepository",
				"central::default::https://lib.iplasso.com/artifactory/libs-release");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_id", "nexus.lfpcode.com");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_repositories_repository_[0]_snapshots_enabled",
				"false");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_repositories_repository_[0]_id",
				"nexus.lfpcode.com-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_repositories_repository_[0]_name",
				"maven-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_repositories_repository_[0]_url",
				"https://nexus.lfpcode.com/repository/maven-releases/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_repositories_repository_[1]_id",
				"nexus.lfpcode.com-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_repositories_repository_[1]_name",
				"maven-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_repositories_repository_[1]_url",
				"https://nexus.lfpcode.com/repository/maven-snapshots/");
		System.setProperty(
				"MAVEN_SETTINGS_XML_profiles_profile_[1]_pluginRepositories_pluginRepository_[0]_snapshots_enabled",
				"false");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_pluginRepositories_pluginRepository_[0]_id",
				"nexus.lfpcode.com-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_pluginRepositories_pluginRepository_[0]_name",
				"maven-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_pluginRepositories_pluginRepository_[0]_url",
				"https://nexus.lfpcode.com/repository/maven-releases/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_pluginRepositories_pluginRepository_[1]_id",
				"nexus.lfpcode.com-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_pluginRepositories_pluginRepository_[1]_name",
				"maven-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_pluginRepositories_pluginRepository_[1]_url",
				"https://nexus.lfpcode.com/repository/maven-snapshots/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_properties_altSnapshotDeploymentRepository",
				"snapshots::default::https://nexus.lfpcode.com/repository/maven-snapshots/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[1]_properties_altReleaseDeploymentRepository",
				"central::default::https://nexus.lfpcode.com/repository/maven-releases/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_id", "nexus.lasso.tm");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_repositories_repository_[0]_snapshots_enabled",
				"false");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_repositories_repository_[0]_id",
				"nexus.lasso.tm-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_repositories_repository_[0]_name",
				"maven-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_repositories_repository_[0]_url",
				"https://nexus.lasso.tm/repository/maven-releases/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_repositories_repository_[1]_id",
				"nexus.lasso.tm-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_repositories_repository_[1]_name",
				"maven-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_repositories_repository_[1]_url",
				"https://nexus.lasso.tm/repository/maven-snapshots/");
		System.setProperty(
				"MAVEN_SETTINGS_XML_profiles_profile_[2]_pluginRepositories_pluginRepository_[0]_snapshots_enabled",
				"false");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_pluginRepositories_pluginRepository_[0]_id",
				"nexus.lasso.tm-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_pluginRepositories_pluginRepository_[0]_name",
				"maven-releases");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_pluginRepositories_pluginRepository_[0]_url",
				"https://nexus.lasso.tm/repository/maven-releases/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_pluginRepositories_pluginRepository_[1]_id",
				"nexus.lasso.tm-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_pluginRepositories_pluginRepository_[1]_name",
				"maven-snapshots");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_pluginRepositories_pluginRepository_[1]_url",
				"https://nexus.lasso.tm/repository/maven-snapshots/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_properties_altSnapshotDeploymentRepository",
				"snapshots::default::https://nexus.lasso.tm/repository/maven-snapshots/");
		System.setProperty("MAVEN_SETTINGS_XML_profiles_profile_[2]_properties_altReleaseDeploymentRepository",
				"central::default::https://nexus.lasso.tm/repository/maven-releases/");
		System.setProperty("MAVEN_SETTINGS_XML_activeProfiles_activeProfile", "artifactory");
		var xmlDocument = buildDocument();
		System.out.println(XMLs.toString(xmlDocument));
	}
}
